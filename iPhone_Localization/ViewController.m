//
//  ViewController.m
//  iPhone_Localization
//
//  Created by Yoshihiro Nozaki on 2014/01/17.
//  Copyright (c) 2014年 mycompany. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)sayHello {
    
    //書式 NSLocalizedString(@"キー名", @"コメント");
    //キー名は、Localizable.stringsファイル内で各言語と紐付く単語です。
    //コメントは、多くの文字列を外国語対応する際に使用します。
    //self.label.text = NSLocalizedString(@"hello", nil);
    self.label.text = NSLocalizedString(@"hello", @"ボタンがタップされた時にラベルを表示する文字列");
}
@end
