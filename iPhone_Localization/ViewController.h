//
//  ViewController.h
//  iPhone_Localization
//
//  Created by Yoshihiro Nozaki on 2014/01/17.
//  Copyright (c) 2014年 mycompany. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *label;
- (IBAction)sayHello;

@end
