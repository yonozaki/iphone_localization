//
//  AppDelegate.h
//  iPhone_Localization
//
//  Created by Yoshihiro Nozaki on 2014/01/17.
//  Copyright (c) 2014年 mycompany. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
